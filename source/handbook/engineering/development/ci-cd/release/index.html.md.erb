---
layout: handbook-page-toc
title: "Release Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

For an understanding of what this team is going to be working on take a look at [the product
vision](/direction/release/).

## Mission

The Release Team is focused on all the functionality with respect to
Continuous Delivery and Release Automation.

This team maps to [Release](/handbook/product/categories/#release) devops stage.

## Team Members

The following people are permanent members of the Release Team:

<%= direct_team(manager_role: 'Engineering Manager, Release (CD)') %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Release/, direct_manager_role: 'Engineering Manager, Release (CD)') %>

## Technologies

Like most GitLab backend teams, we spend a lot of time working in Rails on the main [GitLab CE app](https://gitlab.com/gitlab-org/gitlab-ce), but we also do a lot of work in Go which is used heavily in [GitLab Pages](https://gitlab.com/gitlab-org/gitlab-pages). Familiarity with Docker and Kubernetes is also useful on our team.

## Common Links

 * [Issue Tracker](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Arelease)
 * [Slack Channel](https://gitlab.slack.com/archives/s_release)
 * [Roadmap](/direction/release/)

## How to work with us

### On issues

Issues that contribute to the release stage of the devops toolchain have the `~"devops::release"` label.

### In Slack

The team's primary Slack channel is `#s_release`, and the public managers channel is `#release-managers`. We also support a number of feature channels for discussons or questions about a specific [feature area](/handbook/product/categories/#release-stage). We no longer support issue specific channels, as they are easy to lose track of and fragment the discussion. Supported channels are:

* Continuous delivery: `f_continuous_delivery`
* Release orchestration: `f_release_orchestration`
* Release governance: `f_release_governance`
* Incremental rollout: `f_incremental_rollout`
* Review apps: `f_review_apps`
* Feature flags: `f_feature_flags`
* Merge trains: `f_merge_trains`
* Secrets management: `f_secrets_management`
* Pages: `gitlab-pages`

### Team Workflow

We use the [Release Team Workflow board](https://gitlab.com/groups/gitlab-org/-/boards/1285883?&label_name[]=devops%3A%3Arelease) to plan and track features as they make their way from idea to production. This board details the various stages an issue can exist in as it is being developed. It's not necessary that an issue go through all of these stages, and it is allowed for issues to move back and forward through the workflow as they are iterated on.

#### Workflow Stages

Below is a description of each stage, its meaning, and any requirements for that stage.

* `workflow::start`
  * The entry point for workflow scoped labels. From here, Issues will either move through the validation workflow, go directly to planning and scheduling, or in certain cases, go directly to the development steps.
* `workflow::problem validation`
  * This stage aims to produce a [clear and shared understanding of the customer problem](/handbook/product-development-flow/#validation-phase-2-problem-validation).
* `workflow::solution validation`
  * This output of this stage is a [clear prototype of the solution to be created](/handbook/product-development-flow/#validation-phase-3-solution-validation).
* `workflow::ready for development`
  * This stage indicates the issue is ready for engineering to begin their work.
  * Issues in this stage must have a `UX Ready` label, as well as either `frontend`, `backend` or both labels to indicate which areas will need focus.
* `workflow::in dev`
  * This stage indicates that the issue is actively being worked on by one or more developers.
* `workflow::in review`
  * This stage indicates that the issue is undergoing code review by the development team and/or undergoing design review by the UX team.
* `workflow::verification`
  * This stage indicates that everything has been merged and the issue is waiting for verification after a deploy.

### Engineering Evaluation & Estimation

In order to make sure sprints are effective as possible, it is important to ensure issues are clearly defined and broken down before the start of a sprint. In order to accomplish this, engineering evaluation will take place for issues that require it (small changes and bug fixes typically won’t require this analysis). Engineers will look for assigned issues with the `workflow::planning breakdown` label that have user stories defined, as well as functional, performance, documentation, and security acceptance criteria. See [Build phase](https://about.gitlab.com/handbook/product-development-flow/#build-phase-1-plan) of the Product Development Workflow.

Assigned engineers will work with Product, UX and other engineers to determine the implementation plan for the feature.

Once an implementation plan has been finalized, the following steps should be taken:

* The issue description should be updated to include the details of the implementation plan along with a checklist to show the planned breakdown of merge requests.
* The issue should be moved forward in the workflow to `workflow::ready for development`.
* The weight of the issue should be updated to reflect the number of merge requests estimated as part of the implementation plan. For smaller changes and bugs, the weight should be 1.
* Those updates to the issue will signal that the issue has been properly scoped and is ready to be worked on in an upcoming milestone.

#### Merge request count as a measure of issue weight

As a byproduct of the engineering evaluation process, a rough estimate of the number of merge requests required to develop a feature will be produced. This measure can be used as a way to determine issue weights. These weights can be useful during the planning process to get a rough gauge of how many features can be worked on in a milestone, and to understand how much work the team can do in a milestone. This metric also aligns with the [throughput metric](https://about.gitlab.com/handbook/engineering/management/throughput/) currently measured by engineering teams.

### Async Status Updates

Since we are a [remote](/company/culture/all-remote/) company, we utilize a Slack plugin called [Geekbot](https://geekbot.io/) to coordinate various status updates. There are currently 3 status updates configured in Geekbot, one is weekly and two are daily:

#### Weekly Status Update
The **Weekly Status Update** is configured to run at noon on Fridays, and contains three questions:

1. ***What progress was made on your deliverables this week?*** (MRs and demos are good for this)

    The goal with this question is to show off the work you did, even if it's only part of a feature. This could be a whole feature, a small part of a larger feature, an API to be used later, or even just a copy change.

2. ***What do you plan to work on next week?*** (think about what you'll be able to merge by the end of the week)

    Think about what the next most important thing is to work on, and think about what part of that you can accomplish in one week. If your priorities aren't clear, talk to your manager.

3. ***Who will you need help from to accomplish your plan for next week?*** (tag specific individuals so they know ahead of time)

    This helps to ensure that the others on the team know you'll need their help and will surface any issues earlier.

#### Daily Standup

The **Daily Standup** is configured to run each morning Monday through Thursday and posts to `#g_release` Slack channel. It has just one question:

1. ***Is there anything you could use a hand with today, or any blockers?***

    This check-in is optional and can be skipped if you don't need any help or don't have any blockers. Be sure to ask for help early, your team is always happy to lend a hand.

#### Daily Social

The optional **Daily Social** is configured to run each morning and posts to #g_cicd_social. It has just one question:

1. ***What was something awesome, fun, or interesting that you did yesterday outside of work?***

    This check-in is optional and can be skipped if you don't have anything to share.
